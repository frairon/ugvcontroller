from fabric.api import task, local


@task
def compile():
    local('scons')

@task
def controller(port=4355,
               ip='192.168.178.87'):
    local('python src/controller.py {ugv} {port}'.format(ugv=ip, port=port))


@task
def streamServer():
    #local('cvlc v4l2:// :v4l2-dev=/dev/video0 :v4l2-width=352 :v4l2-height=288 --no-sout-audio '+
    #      '--sout "#transcode{vcodec=mpeg4,vb=800}:standard{access=udp, mux=ts,shaping=0, sap, name=live-video, dst=127.0.0.1, port=1234}"')

    local('mjpg_streamer -i "/usr/local/lib/input_uvc.so -r 320x240 -d /dev/video0 -n -y" -o "/usr/local/lib/output_http.so -p 8090 -w /usr/local/www"')

# mjpg_streamer -i "/usr/local/lib/input_uvc.so -d /dev/video -r 320x240 -n -y -q 20" -o "/usr/local/lib/output_http.so -p 8090 -w /usr/local/www"
#  mjpg_streamer -i "/usr/local/lib/input_uvc.so -r 352x288 -d /dev/video0 -r 320x240 -n -y -q 20" -o "/usr/local/lib/output_http.so -p 8090 -w /usr/local/www"