# README #

This project contains software to run a two-motor car, controlled by gamepad, streaming live video.

The hardware control code is implemented in C using wiringPi (so works on raspberry pi only).
Video streaming is done via mjpeg_streamer.

The controller uses pygame to get gamepad/joystick input, display the video and some control information.
Controller-vehicle communication uses python twisted; the hardware control runs as a separate process to avoid running the network as root.
Network and hardware control communicate via zeromq.

### Setup ###

You'll need a raspberry pi and the hardware setup (schemes will follow someday). On the pi you'll need

* python, libzmq, mjpeg_streamer, wiringPi
* python-twisted, txzmq (twisted-connector for zmq), pythonioc

The controller needs

* python, pygame, numpy (to interpolate steering control)
* python-twisted, pythonioc


### Start the vehicle ###

* compile the `src/hardware.cpp` using libraries `wiringPi, pthread, zmq` or simply run `scons` -- if you have.
* run `python src/vehicle.py`

This will start a server and run the hardware part as root (on raspbian, sudo does not need password by default).
Video streaming will be started by that server on demand.


### Start the controller ##

Simply run `python src/controller.py <target-ip>`