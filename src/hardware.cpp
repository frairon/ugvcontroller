#include <wiringPi.h>
#include <iostream>
#include <softPwm.h>
#include <zmq.hpp>
#include <string>
#include <errno.h>

#define A_PWMB 4
#define A_STBY 15
#define A_BIN1 16
#define A_BIN2 1
#define A_PWMA 7
#define A_AIN1 8
#define A_AIN2 9

#define B_PWMB 29
#define B_STBY 23
#define B_BIN1 24
#define B_BIN2 25
#define B_PWMA 12
#define B_AIN1 14
#define B_AIN2 13

#define PWMRANGE 100

using std::cout;
using std::endl;
using std::string;

typedef enum {
	MOTOR_LEFT = 0, MOTOR_RIGHT, MOTOR_TILT, MOTOR_GRIP,
} MOTOR_IDS;

class MotorState {
public:
	const int id;
	const int pin1;
	const int pin2;
	const int pwm;

private:
	int dir;

public:
	MotorState(int id, int pin1, int pin2, int pwm) :
			id(id), pin1(pin1), pin2(pin2), pwm(pwm), dir(-1) {
	}

	void init() {
		cout << "Initializing motor pins for motor" << this->id << endl;
		pinMode(this->pin1, OUTPUT);
		pinMode(this->pin2, OUTPUT);
		pinMode(this->pwm, OUTPUT);
		digitalWrite(this->pwm, 0);
		int success = softPwmCreate(this->pwm, 1, PWMRANGE);
		if (success !=0){
			cout << "Error creating softpwm " << errno << endl;
		}
	}

	void setDirection(int direction) {
		if (direction == this->dir) {
			return;
		}

		this->dir = direction;

		if (direction) {
			digitalWrite(this->pin1, 1);
			digitalWrite(this->pin2, 0);
		} else {
			digitalWrite(this->pin1, 0);
			digitalWrite(this->pin2, 1);
		}
	}

	void setSpeed(const int speed) {
		cout << "Motor " << this->id << ": Speed -> " << speed << endl;
		softPwmWrite(this->pwm, speed);
	}
};

MotorState Left(MOTOR_LEFT, A_AIN1, A_AIN2, A_PWMA);
MotorState Right(MOTOR_RIGHT, A_BIN1, A_BIN2, A_PWMB);
MotorState Tilt(MOTOR_TILT, B_AIN1, B_AIN2, B_PWMA);
MotorState Grip(MOTOR_GRIP, B_BIN1, B_BIN2, B_PWMB);

void setup() {
	cout << "setting up wiringPi and the pins" << endl;

	wiringPiSetup();

	Left.init();
	Right.init();
	Tilt.init();
	Grip.init();

	cout << "initializing stby-pins" << endl;

	// A-Board
	pinMode(A_STBY, OUTPUT);
	digitalWrite(A_STBY, 1);

	//B-Board
	pinMode(B_STBY, OUTPUT);
	digitalWrite(B_STBY, 1);
}

MotorState * getMotor(const int motor) {

	if (motor == Left.id) {
		return &Left;
	}
	if (motor == Right.id) {
		return &Right;
	}
	if (motor == Tilt.id) {
		return &Tilt;
	}
	if (motor == Grip.id) {
		return &Grip;
	}
	throw 0;
}

void setMotorSpeed(const int motor, const int speed) {
	int absSpeed = abs(speed);
	if (absSpeed < 0 || absSpeed > PWMRANGE) {
		throw 1;
	}
	MotorState *motorState = getMotor(motor);

	if (speed != 0) {
		motorState->setDirection(speed < 0);
	}

	motorState->setSpeed(absSpeed);

}

int main(int argc, char ** argv) {
	setup();

	zmq::message_t replyOk(1);
	memcpy(replyOk.data(), "0", 1);
	zmq::message_t replyErr(1);
	memcpy(replyErr.data(), "1", 1);

	zmq::context_t context(1);
	zmq::socket_t socket(context, ZMQ_REP);
	socket.bind("tcp://*:4533");
	while (true) {
		zmq::message_t request;

		socket.recv(&request);
		try {
			string msg(static_cast<char*>(request.data()), request.size());
			cout << "got message " << msg << endl;

			if (msg == "exit") {
				socket.send(replyOk);
				break;
			}

			int motor = atoi(msg.substr(0, 1).c_str());
			if (msg.at(1) != ':') {
				throw 1;
			}

			int speed = atoi(msg.substr(2).c_str());
			setMotorSpeed(motor, speed);

			socket.send(replyOk);
		} catch (...) {
			cout << "Exception " << endl;
			socket.send(replyErr);
		}
	}

	// stop the chip
	cout << "shutdown" << endl;
	digitalWrite(A_STBY, 0);
	digitalWrite(B_STBY, 0);

	return 0;
}
