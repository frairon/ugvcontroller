from twisted.internet import protocol, reactor
from twisted.protocols import basic

import txzmq
import subprocess
import argparse
import ugv
import pythonioc
import proto
import math
import time
import os

import sys



class VehicleProtocol(proto.MessageProtocol):

    left = '1'
    right = '2'

    maxSpeed = 100

    def messageReceived(self, msg):
        if 'speedLeft' in msg:
            speed = float(msg['speedLeft'])

            self._sendHw('{motor}:{speed}'.format(motor=self.left,
                                                  speed=self._getAbsoluteSpeed(speed)))

        if 'speedRight' in msg:
            speed = float(msg['speedRight'])

            self._sendHw('{motor}:{speed}'.format(motor=self.right,
                                                  speed=self._getAbsoluteSpeed(speed)))

        if 'video' in msg:
            if msg['video'] == 'on':
                self.factory.startVideo()
            else:
                self.factory.stopVideo()


    def _getAbsoluteSpeed(self, relativeSpeed):
        return int(round(min(self.maxSpeed, max(-self.maxSpeed, round(self.maxSpeed * relativeSpeed)))))

    def _sendHw(self, msg):
        print "sending to hardware:", msg
        return self.factory.hwConn.sendMsg(msg)

class ControllerFactory(protocol.ServerFactory):
    protocol = VehicleProtocol

    options = pythonioc.Inject('options')

    def __init__(self):
        pass

    def startFactory(self):
        self._vidProcess = None
        cwd = os.getcwd()

        self._hwProcess = subprocess.Popen(['sudo', os.path.join(cwd, 'hardware')],
                                           stdout=sys.stdout, stderr=sys.stderr)
        def killIfHwFailed():
            if self._hwProcess.poll() is not None:
                print "Cannot start hardware controller"
                reactor.stop()
            else:
                print "Hardware seems to run ok"

        reactor.callLater(1.5, killIfHwFailed)
        self.zf = txzmq.ZmqFactory()
        self.zf.registerForShutdown()

        self.ep = txzmq.ZmqEndpoint(type=txzmq.ZmqEndpointType.connect,
                              address='tcp://localhost:%s' % self.options.hardwarePort)

        self.hwConn = txzmq.ZmqREQConnection(self.zf, self.ep)

        def printReplies(msg):
            print "hardware response: %s" % msg

        self.hwConn.gotMessage = printReplies


    def stopFactory(self):
        self.hwConn.shutdown()

        # kill the hardware process
        if self._hwProcess and self._hwProcess.poll() is None:

            print "Stopping Hardware Controller"

            # we cannot kill it since it's running as sudo,
            # so we spawn a sudo kill with the pid
            killer = subprocess.Popen(['sudo', 'kill', str(self._hwProcess.pid)])
            killer.wait()
            self._hwProcess = None


    def startVideo(self):

        self.stopVideo()
        print "Starting Videostream"

        cmd = ['mjpg_streamer',
            '-i',
            "/usr/local/lib/input_uvc.so -r 640x480 -d %s -n -y" % self.options.videoDev,
            '-o',
            '/usr/local/lib/output_http.so -p %s -w /usr/local/www' % self.options.streamPort]
        print "Starting video stream with: ", ' '.join(cmd)
        self._vidProcess = subprocess.Popen(cmd)

    def stopVideo(self):
        if not self._vidProcess:
            return

        print "Stopping Video Stream"

        self._vidProcess.kill()
        self._vidProcess = None


def main():
    ap = argparse.ArgumentParser(description='Vehicle')
    ap.add_argument('--control-port', dest='ctrlPort',
                    help='Port for controller interface', default=ugv.CTRL_PORT)
    ap.add_argument('--stream-port', dest='streamPort',
                    help='Port for video video stream', default=ugv.STREAM_PORT)
    ap.add_argument('--hardware-port', dest='hardwarePort',
                    help='Port for hardware controlling.', default=ugv.HARDWARE_PORT)
    ap.add_argument('--video-dev', dest='videoDev',
                    help='video device (e.g. /dev/video0)', default='/dev/video0')

    pythonioc.registerServiceInstance(ap.parse_args(), 'options')

    options = pythonioc.Inject('options')

    reactor.listenTCP(options.ctrlPort, ControllerFactory())
    reactor.run()

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise SystemExit(e)

