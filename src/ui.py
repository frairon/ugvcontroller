from SimpleCV import JpegStreamCamera, Display
import pygame
import time
from pgu import gui
import pythonioc
import twutils
from twisted.internet import reactor


class EmbeddedApp(gui.App):
    def event(self, ev):
        if ev.type == pygame.QUIT:
            reactor.stop()
        else:
            gui.App.event(self, ev)

class UserInterface(object):

    vehicleConnection = pythonioc.Inject('vehicleConnection')
    options = pythonioc.Inject('options')

    toolWidth = 200

    videoWidth = 640
    videoHeight = 480

    _camera = None

    def __init__(self):

        sf = pygame.display.set_mode((self.videoWidth + self.toolWidth, self.videoHeight))
        self._vidSurface = sf.subsurface((0, 0, self.videoWidth, self.videoHeight))

        self._app = EmbeddedApp()
        self._app.connect(gui.QUIT, self._app.quit, None)


        main = gui.Table(background=(50, 50, 50))
        main.tr()
        bt = gui.Button("hubs")
        bt.connect(gui.CLICK, self.helloWorld)
        main.td(bt)
        main.tr()
        main.td(gui.Label("some value"))

        self.vidButton = gui.Button("Video")
        self.vidButton.connect(gui.CLICK, self.startCamera)

        main.tr()
        main.td(self.vidButton)

        self._connectionLabel = gui.Label('')
        main.tr()
        main.td(self._connectionLabel)

        self._app.init(widget=main,
                       screen=sf,
                       area=pygame.Rect(self.videoWidth,
                                        0,
                                        self.toolWidth,
                                        self.videoHeight))

    def updateLabels(self):
        if self.vehicleConnection.hasConnection():
            self._connectionLabel.set_text('Connected')
        else:
            self._connectionLabel.set_text('Connecting....')

        print self._connectionLabel.style.width

        self.vidButton.disabled = not self.vehicleConnection.hasConnection()
        self._app.resize()
    def helloWorld(self):
        print "btn clicked"

    def startCamera(self):
        if not self.vehicleConnection.hasConnection():
            return

        connectionString = 'http://{host}:{port}?action=stream'.format(host=self.options.target,
                port=self.options.streamPort)

        try:
            self.vehicleConnection.getConnection().startStream()

            def createCamera():
                print "Trying to create camera"
                self._camera = JpegStreamCamera(connectionString)
                print "success"


            twutils.startRetryingOneshotJob(reactor, createCamera, tryInterval=1)

        except Exception as e:
            self._camera = None
            print e

    def renderStreamImage(self):
        if self._camera:
            img = self._camera.getImage()
            if not img:
                return
            img = pygame.image.frombuffer(img.toString(), img.size(), "RGB")
            if not img:
                return

            self._vidSurface.blit(img, (0, 0))
        else:

            font = pygame.font.SysFont('freemono', 32)
            text = font.render("Video disabled", 1, (200, 200, 200))
            textpos = text.get_rect()
            textpos.centerx = self._vidSurface.get_rect().centerx
            textpos.centery = self._vidSurface.get_rect().centery
            self._vidSurface.blit(text, textpos)

        pygame.display.update(pygame.Rect(0, 0, 640, 480))
    def paint(self):
        self.updateLabels()
        self.renderStreamImage()
        self._app.loop()

