import json
from twisted.protocols import basic


class MessageProtocol(basic.LineReceiver):

    def _encode(self, msg):
        # todo: maybe remove all newlines?
        return json.dumps(msg, separators=(',', ':'))

    def _decode(self, msg):
        # todo: do some checking
        return json.loads(msg)

    def replyError(self, msg):
        self.transport.write(self._encode({'error':str(msg)}))

    def lineReceived(self, line):
        msg = self._decode(line)

        self.messageReceived(msg)

    def messageReceived(self, msg):
        pass

    def sendArgs(self, **kwargs):
        self.sendLine(self._encode(kwargs))
