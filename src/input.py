import pygame
import sys
import math

import numpy

class InputException(Exception):
    pass

def _reduce(value, start, end):
    return min(end, max(start, value))

class Input(object):

    def __init__(self):
        assert pygame.joystick.get_init()

        self._initDriverCurves()

        if pygame.joystick.get_count() < 1:
            raise InputException("No Joystick found")

        self._j = pygame.joystick.Joystick(0)
        self._j.init()


        self.left = 0
        self.right = 0
        self.dir = 0
        self.speed = 0


    def _initDriverCurves(self):
        #
        # some defined points at the curve
        pi = math.pi
        pi2 = math.pi / 2.0
        pi4 = math.pi / 4.0
        pi34 = math.pi * 0.75

        # all steps
        self._driverTicks = [-pi, -pi34, -pi2, -pi4, 0, pi4, pi2, pi34, pi]

        self._leftDriver = [-1, -1, -1, 0, 1, 1, 1, 0, -1]
        self._rightDriver = [-1, 0, 1, 1, 1, 0, -1, -1, -1]

    def update(self):
        # get both axis values, inverting y-axis
        yAxis = self._j.get_axis(1) * -1
        xAxis = self._j.get_axis(0)

        # calculate direction as phi
        dir = numpy.arctan2(xAxis, yAxis)
        dir = _reduce(dir, -math.pi, math.pi)

        # get speed and reduce to interval [-1, 1]
        speed = math.sqrt(xAxis * xAxis + yAxis * yAxis)
        speed = _reduce(speed, -1, 1)

        # check if values have changed
        changed = speed != self.speed or dir != self.dir

        # save values for later
        self.speed = speed
        self.dir = dir


        # get interpolated values
        leftDir = numpy.interp(self.dir, self._driverTicks, self._leftDriver)
        rightDir = numpy.interp(self.dir, self._driverTicks, self._rightDriver)
        leftDir = _reduce(leftDir, -1, 1)
        rightDir = _reduce(rightDir, -1, 1)

        self.left = numpy.round(leftDir * self.speed, 3)
        self.right = numpy.round(rightDir * self.speed, 3)

        # set to 0 if smaller below threshold
        self.left = 0 if abs(self.left) < 0.1 else self.left
        self.right = 0 if abs(self.right) < 0.1 else self.right

        return changed