import pygame
from twisted.internet import task, reactor
import sys
import input
import argparse
from twisted.internet import protocol

import proto
import ugv
import pythonioc
import ui

DESIRED_FPS = 10

class ClientProtocol(proto.MessageProtocol):

    def connectionMade(self):
        self.factory.connection = self

    def connectionLost(self, reason):
        self.factory.connection = None

    def startStream(self):
        self.sendArgs(video='on')

    def stopStream(self):
        self.sendArgs(video='off')


class VehicleFactory(protocol.ReconnectingClientFactory):
    protocol = ClientProtocol
    connection = None

    def hasConnection(self):
        return self.connection is not None

    def getConnection(self):
        return self.connection

    def buildProtocol(self, addr):
        self.resetDelay()
        return protocol.ReconnectingClientFactory.buildProtocol(self, addr)

class Controller(object):
    input = pythonioc.Inject('input')
    gui = pythonioc.Inject('gui')
    connection = pythonioc.Inject('vehicleConnection')
    def __init__(self):
        pass


    def tick(self):

        movementChanged = self.input.update()
        if self.connection.hasConnection():
            conn = self.connection.getConnection()
            if movementChanged:
                conn.sendArgs(speedLeft=self.input.left, speedRight=self.input.right)

        self.gui.paint()


def globalInit():
    pygame.init()

def main():

    options = pythonioc.Inject('options')

    vehicleFactory = VehicleFactory()
    pythonioc.registerServiceInstance(vehicleFactory, 'vehicleConnection')

    reactor.connectTCP(options.target, options.ctrlPort, vehicleFactory)

    gui = ui.UserInterface()
    pythonioc.registerServiceInstance(gui, 'gui')

    controller = Controller()
    tick = task.LoopingCall(controller.tick)
    tick.start(1.0 / DESIRED_FPS)

    reactor.run()

if __name__ == '__main__':
    globalInit()
    ap = argparse.ArgumentParser(description='Ugv Controller')
    ap.add_argument('target', help='IP of the UGV')
    ap.add_argument('--control-port', dest='ctrlPort',
                    help='Port for controller interface', default=ugv.CTRL_PORT)
    ap.add_argument('--stream-port', dest='streamPort',
                    help='Port for video video stream', default=ugv.STREAM_PORT)


    pythonioc.registerServiceInstance(ap.parse_args(), 'options')
    pythonioc.registerServiceInstance(input.Input(), 'input')

    try:
        main()
    except Exception as e:
        raise SystemExit(e)
